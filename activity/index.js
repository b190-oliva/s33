// ==== ACTIVITY ====
// Retrieve all to-do-lists
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => console.log(json))

// returns just the titles of every item
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {data.map((json) => {console.log(json.title)})})

// retrieving single result on to do list items

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json))


// printing title and status of the to do list item

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => {return response.json()})
.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))

// fetch request using the POST method that will create a to do list item

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		completed: false,
		title: "Created To Do List Item!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// updating to do list item (PUT)

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure",
		id: 1,
		status:"Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// updating to do list item (PATCH)

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "07/09/22",
		id: 1,
		status:"Complete",
		title: "delectus aut autem",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// delete method


fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
});