

// Section - JS Synchronous vs Asynchronous
/*
	JavaScript is by default is synchronous
	- it will only excute one at a time, from top to bottom.
*/


// console.log("Hello philippines");
// // conosle.log("Hello again!");
// console.log("Goodbye");

// for(let i = 1; i <= 1500; i++){
// 	console.log(i);
// };
// console.log("It's me again!");

// // console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// fetch("https://jsonplaceholder.typicode.com/posts")
// // .then(response => console.log(response));
// .then(response => response.json())
// .then(json => console.log(json));
// console.log("Hello");

// // ASYNC-AWAIT
// // "async" and "await" keywords are other approach that can be used to perform asynchronous javaScript
// // used in functions to indicate which portions of the code should be waited for 

// // creates an asynchronous function
async function fetchData(){
	// waits for the fetch method to complete then stores it inside the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	console.log(result);
	console.log(typeof result);
	console.log(result.body);
	//converts the data from the response object inside the result variable as JSON
	let json = await result.json();
	// then it prints out the content of the response object
	console.log(json);
};

// fetchData();
// console.log("Hello");

// Section creating a post
/*
	SYNTAX:
		fetch(URL, options)
*/


// create a new post following REST API (create, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
	// setting request method into "POST"
	method: "POST",
	// setting request headers to be sent to the backend
	// specified that the content headers will be sending JSON structure for it's content
	headers: {
		"Content-Type": "application/json"
	},
	// set content/body data of the "request" object to be sent to the backend
	// JSON.stringify converts the object into stringified JSON
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));


// mini activity
// SECTION - update a post (PUT)
// updates a specific post following the RES API (update, /post/:id, PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "Updated Post",
		body: "Hello again!",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

//SECTION - update a post (PATCH)
// difference between patch and put is the number of properties being updated, patch used to update a single property while maintaining the unupdated properties
// put in the other hand is used when all of the properties need to be updated, or the whole document itself.
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title:"Corrected Post"
	})
})
.then(response => response.json())
.then(json => console.log(json));

// SECTION - deleting of a resource

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method: "DELETE"
})

// SECTION - filter posts

/*
	Syntax:
	"<url>?parameterName=value"
	"<url>"?parameterNameA=valueA&&parameterNameB=ValueB" this is used for multiple parameter search
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response => response.json())
.then(json => console.log(json));

// Retrieving comments for a specific post / accessing nested/embedded comments
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json));
